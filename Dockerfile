FROM ubuntu:18.04

ENV DEBIAN_FRONTEND=noninteractive
ENV LANG=C.UTF-8

COPY install-pkgs.sh /install-pkgs.sh

RUN bash /install-pkgs.sh

ENV gvm_libs_version="v11.0.0" \
    openvas_smb="v1.0.5" \
    openvas_scanner_version="v7.0.0"


RUN echo "Starting Build..." && mkdir /build

    #
    # install libraries module for the Greenbone Vulnerability Management Solution
    #

RUN cd /build && \
    wget https://github.com/greenbone/gvm-libs/archive/$gvm_libs_version.tar.gz && \
    tar -zxvf $gvm_libs_version.tar.gz && \
    cd /build/*/ && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release .. && \
    make && \
    make install && \
    cd /build && \
    rm -rf *

    #
    # install smb module for the OpenVAS Scanner
    #

RUN cd /build && \
    wget https://github.com/greenbone/openvas-smb/archive/$openvas_smb.tar.gz && \
    tar -zxvf $openvas_smb.tar.gz && \
    cd /build/*/ && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release .. && \
    make && \
    make install && \
    cd /build && \
    rm -rf *

    #
    # Install Open Vulnerability Assessment System (OpenVAS) Scanner
    #

RUN cd /build && \
    wget https://github.com/greenbone/openvas-scanner/archive/$openvas_scanner_version.tar.gz && \
    tar -zxvf $openvas_scanner_version.tar.gz && \
    cd /build/*/ && \
    mkdir build && \
    cd build && \
    cmake -DCMAKE_BUILD_TYPE=Release .. && \
    make && \
    make install && \
    cd /build && \
    rm -rf *

RUN echo "/usr/local/lib/" > /etc/ld.so.conf.d/usr-local-lib.conf && \
	chmod 0644 /etc/ld.so.conf.d/usr-local-lib.conf && \
	ldconfig

COPY start.sh /start.sh

CMD '/start.sh'
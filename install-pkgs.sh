#!/usr/bin/env bash

apt-get update

{ cat <<EOF
bison
build-essential
ca-certificates
cmake
curl
dirb
gcc
gcc-mingw-w64
geoip-database
git
heimdal-dev
ike-scan
libgcrypt20-dev
libglib2.0-dev
libgnutls28-dev
libgpgme-dev
libgpgme11-dev
libhiredis-dev
libksba-dev
libmicrohttpd-dev
libnet-snmp-perl
libpcap-dev
libpopt-dev
libsnmp-dev
libssh-gcrypt-dev
libxml2-dev
ncrack
net-tools
nmap
perl-base
pkg-config
redis-server
redis-tools
rsync
smbclient
uuid-dev
wapiti
wget
EOF
} | xargs apt-get install -yq --no-install-recommends

rm -rf /var/lib/apt/lists/*

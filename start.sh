#!/usr/bin/env bash

if [ ! -d "/run/redis" ]; then
	mkdir /run/redis
fi
redis-server --unixsocket /run/redis/redis.sock --unixsocketperm 700 --timeout 0 --databases 2048 --maxclients 100000 --daemonize yes --port 0 --bind 127.0.0.1

echo "Testing redis status..."
X="$(redis-cli -s /run/redis/redis.sock ping)"
while  [[ "${X}" != "PONG" ]]; do
        echo "Redis not yet ready..."
        sleep 1
        X="$(redis-cli -s /run/redis/redis.sock ping)"
done
echo "Redis ready."

if [ ! -f /firstrun ]; then
    useradd --home-dir /usr/local/share/openvas openvas-sync
    chown openvas-sync:openvas-sync -R /usr/local/share/openvas
    chown openvas-sync:openvas-sync -R /usr/local/var/lib/openvas
    touch /firstrun
fi

touch /usr/local/var/log/gvm/openvas.log

echo "Tailing logs"
tail -F /usr/local/var/log/gvm/*
